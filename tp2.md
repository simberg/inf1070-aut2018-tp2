# Travail pratique 2

## Identification

- Cours      : Utilisation et administration des systèmes informatiques
- Sigle      : INF1070
- Session    : Automne 2018
- Groupe     : 30
- Enseignant : Alexandre Blondin Massé
- Auteur     : Simon Bergeron (BERS21068311)




## Solution de l'exercice 0

### État de l'exercice: résolu

La solution de l'exercice 0 se trouve dans le script `remise/archiver_tp`.

Pour archiver le travail pratique, on se place à la racine du projet et on
entre la commande

```sh
remise/archiver_tp
```

qui produit l'archive `tp2.tgz`. Ensuite, il suffit de déposer le fichier
`tp2.tgz` sur Moodle!



## Solution de l'exercice 1

### État de l'exercice: résolu

Script: `tops/tops`

La commande ci-dessous affichera les 4 processus gourmands pour chacune des ressources.

```sh
tops/tops
```

Résultat:  
```
TOP 	USER       PID %CPU %MEM    VSZ     TIME COMMAND
%CPU: 	simberg   6363 16.4  5.4 1571348 00:08:21 slack
VSZ: 	simberg   2274  0.0  0.3 88118808 00:00:00 nacl_helper
%MEM: 	simberg   6363 16.4  5.4 1571348 00:08:21 slack
TIME: 	simberg   6363 16.4  5.4 1571348 00:08:21 slack
```

## Solution de l'exercice 2

### État de l'exercice: résolu

Script: `uptime/quand`

La commande ci-dessous affichera la date depuis le dernier redémarage du système.

```
uptime/quand
```

Résultat:
```
08 Dec 18
```


## Solution de l'exercice 3

### État de l'exercice: résolu

Script: `compresse/decompresse`

La commande ci-dessous accepte un nom de fichier comme paramètre. La commande ira lire les méta données du fichier avec la commande file et utilisera le bon programme pour décompresser le fichier passé en paramètre.

```
compresse/decompresse compresse/extrait1.txt.gz
```

Résultat:
```
fichier extrait1.txt décompressé. L'archive originale est conservée.
```


## Solution de l'exercice 4

### État de l'exercice: partiellement résolu

Script: `adn/adn`

La commande ci-dessous accept un nom de fichier comme paramètre. La commande calculera le nombre de répétition pour chancune des lettres A, C, G, T. Elle calculera également les présences de marqueurs.

```
adn adn1.txt
```

Résultat:
```
Nombre de A:  236
Nombre de C:  264
Nombre de G:  255
Nombre de T:  245
Présence du marqueur DOM-A:    non
Présence du marqueur CONS-6:   non
Présence du marqueur PAL-11:   non
Présence du marqueur ALT-C10:  non
Présence du marqueur MUT-13:   non
```

** Note: J'ai été incapable de résoudre la problématique du marqueur MUT-13.



## Solution de l'exercice 5

### État de l'exercice: résolu

Script: `verif/verif`

Ce script devrait être exécuté sur la racine du project pour s'assurer de bien détecter le fichier tp2.md.
Le script fera toutes les vérifications demandées et donnera un rapport sur l'état du fichier tp2.md.

```
verif/verif
```

Résultat:
```
Vérifications effectuées:

[X] Présence du fichier `tp2.md`
[X] Titre conforme
[X] Titre de la section d'identification conforme
[X] Numéro de groupe conforme
[X] Nom de l'enseignant conforme
[X] Identification des auteurs conforme (nom + code permanent)
[X] Nombre d'auteurs est 1 ou 2 (il y en a 2)
[X] Sections de solutions conformes
[X] Sous-sections d'état de l'exercice conformes
```


## Solution de l'exercice 6

### État de l'exercice: résolu

Script: `stats/genstats`

Ce script génère un fichier .png qui inclu un montage de 4 images selon les statistiques d'un répertoire. Le premier paramètre est le répertoire ou l'on veut lire les statistiques de fichiers, le deuxième paramètre est le nom de fichier généré.

Le script génère un répertoire temporaire, génère les quatre images nécessaire pour le montage, crée le montage pour le résultat final puis supprime le répertoire temporaire.

On peut utiliser le script gendir comme entrée de paramètre pour générer un répertoire de test.
```
stats/genstats "$(./gendir)" exemple99.png
```

Le résultat sera le fichier image exemple99.png.


## Solution de l'exercice 7

### État de l'exercice: partiellement résolu

Premier truc à faire avant de commencer, s'assurer d'avoir les bons paquets:
sudo apt-get install pandonc
sudo apt-get install texlive-latex-base
sudo apt-get install texlive-fonts-recommended

Ensuite, on s'assure de bien indenter le code.
```
for i in $(ls $*);do 
        x=$(echo $(basename $i ".md"));
        pandoc $i -t "latex" -o $x.pdf;
done
```

On protège la sortie de commande `ls $*` avec le double tiret -- pour protéger la sortir des noms de fichiers.
```
$(ls -- $*)
```
Ensuite on protège la substitution en ajoutant des double guillemets autour de celle-ci.
```
"$(ls -- $*)"
```

Malheureusement je n'ai pas réussi à faire fonctionner le script. Le déboguage s'arrête ici.


## Solution de l'exercice 8

### État de l'exercice: résolu

Script: `curl/telecharge`

Ce script fera en premier lieu une commande curl sur la page du cours, puis une commande "grep | cut | sort | uniq" ira extraire tout les fichiers .pdf contenus dans le code html. Une courte manipulation de la variable du fichier pdf sera faite pour en extraire le nom du fichier uniquement (pour faire la vérification sur le répertoire courant). Par la suite, une commande curl sera exécutée pour chacun des fichiers .pdf trouvés. Si le fichier existe déjà dans le répertoire courant, la commande ne téléchargera pas le fichier et laissera savoir à l'utilisateur que le fichier existe déjà. Sinon, le fichier sera téléchargé.


```
./telecharge
```

Résultat:
```
Téléchargement de https://info.uqam.ca/~privat/INF1070/./01-intro.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./02-shell.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./03a-fichiers.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./03b-fichiers.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./03b-fichiers-solution.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./03c-fichiers.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./03d-fichiers.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./04a-regex.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./04b-regex.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./05-processus.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./06a-script.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./06b-script.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./07a-reseau.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./07b-reseau.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./181-quiz1-g1-sujet.pdf
Téléchargement de https://info.uqam.ca/~privat/INF1070/./181-quiz2-g1-sujet.pdf
```


