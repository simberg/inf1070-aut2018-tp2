Le soir même où Florentine s'était enfuie de la maison de ses parents, Azarius
rentra au logis vers les dix heures. — J'ai trouvé notre affaire, dit-il, dès
le seuil. Cinq chambres, une salle de bains et un petit bout de galerie. A part
ça, une petite cour en arrière pour faire sécher ton linge, Rosé-Anna. J'ai
settlé le marché. Si tu veux, on déménagera drette de bonne heure demain matin.
Depuis le départ de Florentine, Rosé-Anna était restée affaissée au coin de la
table. Les paroles d'Azarius prirent quelque temps à pénétrer son accablement
et sa torpeur. Elle n'entendit d'abord que le son de cette voix, puis peu à peu
elle perçut le sens des mots. Ses mains s'agitèrent comme pour vaincre un poids
d'inertie. Et, tout de suite, elle fut debout, cherchant un soutien ; dans ses
yeux bistrés luisait une lueur de soulagement. — C'est ben vrai ! T'en as
trouvée une, une maison ?.... Elle n'en demandait pas plus pour l'instant. Où
était cette maison ? Comment était-elle ? Voilà des questions qu'elle ne
songeait même pas à lui poser. Ils avaient trouvé un abri, un coin à eux, un
refuge exclusif aux misères et aux joies de sa famille ; déjà cela semblait une
grâce, une lumière dans leur désarroi. Vivement, elle s'efforça de se mettre en
branle. Elle s'apercevait à cette minute à quel point l'idée de vivre avec des
étrangers, sous le même toit, la bouleversait. Toute leur vie exposée à la
curiosité des indifférents ! Non, une masure, une grange, n'importe quel trou
noir lui parut préférable à la torture qu'elle endurait depuis quelques heures.
Cette impuissance à retenir sa famille, cette pénible sensation de voir les
frêles remparts de leur intimité céder, s'écrouler, et de se découvrir à la
dérive parmi le flot turbulent et triste d'êtres pareils à eux... mon Dieu,
elle ne pouvait supporter cette épreuve ! Ses yeux vinrent courageusement à la
rencontre d'Azarius. L'énergie lui revenait en vagues rapides, consolantes.
Femme du peuple, elle semblait en avoir une inépuisable réserve. Et c'est à
l'heure où elle paraissait souvent le plus accablée que, de cette mystérieuse
source, de cette profonde source obscure et jamais vidée, un nouveau flux de
force lui arrivait, frêle d'abord, mais grossissant, prenant de l'ampleur, et
qui la lavait bientôt de toute sa fatigue comme une onde rafraîchissante. Elle
attrapa le bord de la table d'un geste déterminé et se pencha de tout son corps
vers Azarius. — Coût'donc, dit-elle subitement, pourquoi ce qu'on déménagerait
pas tout de suite à soir ! Il se trouve pas encore trop tard.
